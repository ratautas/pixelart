﻿If you do not have sketch app on your machine you can view and read design information using following web apps:

Avocode - have free version, no desktop app require
https://avocode.com/ 

Zeplin - have free version, require desktop app to upload design
https://app.zeplin.io
