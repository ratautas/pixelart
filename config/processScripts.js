const pkg = require('../package.json');
const hasJquery = require('./utils/hasJquery');

module.exports = function processScripts(mode) {
  const babelPresets = [
    ['@babel/preset-env', {
      modules: false,
      corejs: 3,
      useBuiltIns: 'usage',
      targets: {
        browsers: mode !== 'legacy' ? Object.values(pkg.browserslist.modern) : Object.values(pkg.browserslist.legacy),
        // browsers: Object.values(pkg.browserslist.legacy),
      },
    }],
  ];

  const babelPlugins = [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-class-properties',
  ];

  const scriptRules = [{
    test: /\.tsx?$/,
    exclude: /(node_modules|bower_components|modules\/system)/,
    // exclude: /(bower_components|modules\/system)/,
    loader: 'awesome-typescript-loader',
    options: {
      sourceMaps: true,
      babelCore: '@babel/core',
      // removeComments: false,
      babelOptions: {
        babelrc: true,
        plugins: babelPlugins,
        presets: babelPresets,
      },
      configFileName: './config/tsconfig.json',
      useBabel: true,
    },
  }, {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components|modules\/system)/,
    // exclude: /(bower_components|modules\/system)/,
    use: {
      loader: 'babel-loader',
      options: {
        // removeComments: false,
        sourceMaps: true,
        cacheDirectory: true,
        plugins: babelPlugins,
        presets: babelPresets,
      },
    },
  }];

  if (mode !== 'development') {
    if (hasJquery()) {
      scriptRules.push({
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: '$',
        }],
      });
    }
  } else {
    scriptRules.push({
      test: /\.jsx?$/,
      loader: 'eslint-loader',
      enforce: 'pre',
      // exclude: /(node_modules|bower_components|modules)/,
      exclude: /(node_modules|bower_components|modules\/system)/,
      options: {
        cache: true,
        // fix: true,
      },
    }, {
      test: /\.tsx?$/,
      loader: 'tslint-loader',
      enforce: 'pre',
      exclude: /(node_modules|bower_components|modules\/system)/,
      options: {
        // cache: true,
        cache: false,
        // fix: true,
        configFile: './config/tslint.json',
      },
    });
  }

  return scriptRules;
};
