const addToBody = (cls: string) => document.body.classList.add(cls);

const device = {
  isSet: false,
  isPortrait: screen.width < screen.height,
  ratio: screen.width / screen.height,
  isTouch: typeof window.ontouchstart !== 'undefined',
  isMob: screen.width < 768,
  isTablet: screen.width < 1025 && screen.width > 767,
  isLaptop: screen.width > 1024,
  winW: window.innerWidth,
  winH: window.innerHeight,
  bodyW: document.body.clientWidth,
  bodyH: document.body.clientHeight,
  scrollbarW: window.innerWidth - document.body.clientWidth,
  hasScrollbar: document.body.clientWidth !== window.innerWidth,
  isIOS: ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0,
  OS: null,
  ua: window.navigator.userAgent,
  init () {
    if (this.ua.indexOf('Windows NT 10.0') !== -1) this.OS = 'win-10';
    if (this.ua.indexOf('Windows NT 6.2') !== -1) this.OS = 'win-8';
    if (this.ua.indexOf('Windows NT 6.1') !== -1) this.OS = 'win-7';
    if (this.ua.indexOf('Windows NT 5.1') !== -1) this.OS = 'win-xp';
    if (this.ua.indexOf('Linux') !== -1) this.OS = 'linux';
    if (this.ua.indexOf('Mac') !== -1) this.OS = 'mac-os';

    addToBody(this.isIOS ? 'is-ios' : 'no-ios');
    addToBody(this.isTouch ? 'is-touch' : 'no-touch');
    addToBody(this.isMob ? 'is-mob' : 'no-mob');
    addToBody(this.isTablet && this.isTouch ? 'is-tablet' : 'no-tablet');
    addToBody(this.isLaptop ? 'is-laptop' : 'no-laptop');
    addToBody(this.OS);
    addToBody(
      !!this.ua.match(/Version\/[\d\.]+.*Safari/) && this.isTouch ? 'mob-safari' : 'no-safari',
    );

    if (this.ua.indexOf('Windows') !== -1) addToBody('has-scrollbar');
  },
  check () {
    this.isSet = false;
    this.winW = window.innerWidth;
    this.winH = window.innerHeight;
    this.bodyW = document.body.clientWidth;
    this.bodyH = document.body.clientHeight;
    this.ratio = window.innerWidth / window.innerHeight;
    this.scrollbarW = window.innerWidth - document.body.clientWidth;
    this.hasScrollbar = document.body.clientWidth !== window.innerWidth;
  },
  set () {
    if (!this.isSet) {
      this.isSet = true;
      setTimeout(() => this.check(), 100);
    }
  },
};

export default device;
