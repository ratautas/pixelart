import Tweener from './Tweener';

class TweenerFade extends Tweener {
  public addTweens() {
    super.addTweens(); // Inherit methods form parent class.
    this.addTween(this.$tweener, 1.5, { y: window.innerHeight / 10 });
    return this;
  }
}

export default TweenerFade;
