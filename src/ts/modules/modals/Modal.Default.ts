import animejs from 'animejs';

import Modal from './Modal';

class ModalDefault extends Modal {
  onOpen() {
    animejs.set(this.$modal, { visibility: 'visible' });
    animejs({
      targets: this.$modal,
      opacity: [0, 1],
      easing: 'easeOutQuad',
      duration: 800,
      complete: () => this.afterOpen(),
    });
    return this;
  }

  onClose() {
    animejs({
      targets: this.$modal,
      opacity: [1, 0],
      easing: 'easeOutQuad',
      duration: 800,
      complete: () => this.afterClose(),
    });
    return this;
  }

  afterClose() {
    animejs.set(this.$modal, { visibility: 'hidden' });
    return this;
  }
}

export default ModalDefault;
