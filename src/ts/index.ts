// SCSS
import '../scss/index.scss';

// POLYFILLS
// Custom POLYFILLS
import './toolkit/polyfills';
import device from './toolkit/device';
device.init();

// Vendor POLYFILLS and initialization
import 'intersection-observer';
import cssVarsPonyfill from 'css-vars-ponyfill';
cssVarsPonyfill();

// Universal MODULES:
import VLDController from './modules/vld/VLDController';

// Varios other TOOLS:
import $$ from './toolkit/$$';
import { fixBody, releaseBody } from './toolkit/fixBody';

const initApp = () => {
  // initialize forms:
  new VLDController();

  // initializes observer instead of scroll listener
  if ($$('[data-observer]')[0]) {
    new IntersectionObserver((entries: IntersectionObserverEntry[]) => {
      const { isIntersecting } = entries[0];
      document.body.classList[isIntersecting ? 'remove' : 'add']('header-narrow');
    }).observe($$('[data-observer]')[0]);
  }

  if ($$('[data-navicon]')[0]) {
    $$('[data-navicon]')[0].addEventListener('click', () => {
      const navOpen = document.body.classList.contains('nav-open');
      document.body.classList[navOpen ? 'remove' : 'add']('nav-open');
      navOpen ? releaseBody() : fixBody();
    });
  }
};

window.addEventListener('load', initApp);
