// SCSS
import '../scss/index.scss';

// Vendor POLYFILLS and initialization
import 'whatwg-fetch';
import cssVarsPonyfill from 'css-vars-ponyfill';

// Various other TOOLS:
import $$ from '../ts/toolkit/$$.ts';

cssVarsPonyfill();


const initApp = () => {
  // console.log('init!');

  if ($$('[data-observer]')[0]) {
    new IntersectionObserver((entries) => {
      const {
        isIntersecting,
      } = entries[0];
      document.body.classList[isIntersecting ? 'remove' : 'add']('header-narrow');
    }).observe($$('[data-observer]')[0]);
  }
};

if ($$('[data-navicon]')[0]) {
  $$('[data-navicon]')[0].addEventListener('click', () => {
    document.body.classList.toggle('nav-open');
  });
}


window.addEventListener('load', initApp);
